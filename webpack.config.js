var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: __dirname + '/src/js/entry.js',
    output: {
        path: __dirname + '/dist/js/',
        filename: 'bundle.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: __dirname + '/dist/index.html',
            template: __dirname + '/src/index.html'
        })
    ],
    module: {
        loaders: [
            {
                test: /\.hbs/,
                loader: "handlebars-template-loader"
            }
        ]
    }
}