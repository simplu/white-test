/*
 * This is just to serve the blank index.html file that was added in the dist folder
 */
var express = require('express');
var app = express();
var path = require('path');

app.use(express.static('dist'));
app.use(express.static('public'));


app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});


app.listen(process.env.PORT || 5000);