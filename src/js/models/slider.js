var backbone = require('backbone');
var _ = require('underscore');
module.exports = Backbone.Model.extend({
    parse: function(data){


        //Split array into chunks/blocks of 4
        var n = 4;
        data = _.groupBy(data, function(element, index){
            return Math.floor(index/n);
        });
        data = _.toArray(data); //Added this to convert the returned object to an array.


        return {
            slides: data,
            settings: {
                position: 0,
                slide: 0
            }
        };
    }
});