var Handlebars = require('handlebars-template-loader/runtime');


Handlebars.registerHelper('increment', function(value, options) {
    return parseInt(value) + 1;
});



Handlebars.registerHelper('random_url', function(value, options) {

    var totalImages = value.length;
    var randomNumber = Math.floor(Math.random() * totalImages - 1) + 1;

    return value[randomNumber].url
});

