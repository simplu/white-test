var Mn = require('backbone.marionette');
var $ = require('jquery');

var sliderTemplate =  require('./templates/slider.hbs');



module.exports = Mn.View.extend({
    template: sliderTemplate,
    events: {
        'click .js-next:not(.is-disabled)': 'nextSlide',
        'click .js-prev:not(.is-disabled)': 'prevSlide',
        "click .js-page-link": 'pageLinkClick'
    },

    initialize: function(){
        var self = this;
        this.model.fetch().done(function() {
            self.render();

            if(self._placeHolder) {
                $(self._placeHolder).html(self.$el);
            } else {
                $('#app').html(self.$el)
            }

        });
    },


    checkSliderNavigationButtons: function(){

        var model = this.model.attributes;
        var slideNumber = model.settings.slide;
        var totalSlides = model.slides.length;
        var nextButton = this.$el.find('.js-next');
        var prevButton = this.$el.find('.js-prev');


        //Prev / Next disable/enable
        if(slideNumber + 1 === totalSlides) {
            nextButton.addClass('is-disabled')
        } else {
            nextButton.removeClass('is-disabled')
        }

        if(slideNumber === 0) {
            prevButton.addClass('is-disabled')
        } else {
            prevButton.removeClass('is-disabled')
        }


        //Current page
        this.$el.find(".js-page-link").removeClass('is-active');
        this.$el.find(".js-page-link[data-id='" + slideNumber + "']").addClass('is-active');
    },

    goToSlideNo: function(slideNumber){
        var speed = 200;
        var self = this;
        var moveWidth = this.$el.find('.js-slide').width();
        self.$el.find('.js-scroll').animate({
            left: - (moveWidth * slideNumber)
        }, speed, function(){
            self.model.set({
                settings: {
                    position: moveWidth * slideNumber,
                    slide: slideNumber
                }
            });
            self.checkSliderNavigationButtons();
        });
    },

    nextSlide: function() {
        var slide = this.model.attributes.settings.slide;
        this.goToSlideNo(slide + 1);
    },

    prevSlide: function() {
        var slide = this.model.attributes.settings.slide;
        this.goToSlideNo(slide - 1);
    },

    pageLinkClick: function(e){

        if(e) {e.preventDefault()}

        var $this = $(e.currentTarget);
        var pageToGoTo = parseInt($this.attr('href'));

        this.goToSlideNo(pageToGoTo)

    }
});

