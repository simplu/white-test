require("./helpers/helpers.js");
var SliderView = require('./Slider');
var SliderModel = require('./models/slider.js');


var sliderModel1 = SliderModel.extend({
    url: 'mock.json' //Generated with https://beta.json-generator.com/V1-Ij0f2m
});
var sliderModel2 = SliderModel.extend({
    url: 'mock2.json' //Generated with https://beta.json-generator.com/V1-Ij0f2m
});

var slider = SliderView.extend({
    model: new sliderModel1()
});

var slider2 = SliderView.extend({
    model: new sliderModel2(),
    _placeHolder: '#slider2'
});


new slider();
new slider2();